Field Empty Text adds an "Empty Text" configuration option for any entity field.
The module behaves similarly to a Views "No Results Behvior" for fields.

Visit the Edit page of the field you'd like to configure and provide the text.

If a field is empty and is configured to be displayed in a view mode, the
configured empty text will display instead. The text displayed is translatable.

Field Empty Text plays nicely with Features, as the configuration is stored in
the field instance configuration. This is automatically picked up.
